/**
 * The AODataFactory class handles the type of AOData to use
 */
package net.aocraft.aosuite.data;

import net.aocraft.aosuite.AOSuite;

public class AODataFactory {
	
	 public static AOData getDataHandler() {
		 // Currently only AOSQL is available for data handling
		 // Other data factory handlers can be easily added later on down the road
		 if (AOSuite.getDataType()=="SQL") {
			return SQLDataFactory.getSQLDataHandler();
		 }
         return null;
         
        
   }

}
